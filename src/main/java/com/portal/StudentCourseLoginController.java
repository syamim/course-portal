package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public class StudentCourseLoginController implements Initializable {
	@FXML private TextField courseid;
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}

	/**
	 * Redirect user to main menu
	 * @throws IOException
	 */
	@FXML 
	private void backMenu() throws IOException {
		App.setRoot("student/menu");
	}

	/**
	 * Redirect user to business course info
	 * @throws IOException
	 */
	@FXML 
	private void redirectBusiness() throws IOException {
		App.setRoot("student/course_Business");
	}

	/**
	 * Redirect user to computing course info
	 * @throws IOException
	 */
	@FXML 
	private void redirectComputing() throws IOException {
		App.setRoot("student/course_Computing");
	}

	/**
	 * Redirect user to law course info
	 * @throws IOException
	 */
	@FXML 
	private void redirectLaw() throws IOException {
		App.setRoot("student/course_Law");
	}
	
	/**
	 * Redirect user to engineering course info
	 * @throws IOException
	 */
	@FXML 
	private void redirectEngineering() throws IOException {
		App.setRoot("student/course_Engineering");
	}
	
	/**
	 * Redirect user to technology course info
	 * @throws IOException
	 */
	@FXML 
	private void redirectTechnology() throws IOException {
		App.setRoot("student/course_Technology");
	}
}

