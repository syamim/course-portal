package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

public class FacultyEditFormController implements Initializable{
  String folder = "faculty_admin/";
  Course current_course;
  @FXML Label courseName,msg,title,subtitle;
  @FXML Button backMain, updateCourse;
  @FXML TextArea aboutField, employmentField, foundationCourseField, foundationScholarshipField;
  @FXML TextArea degreeCourseField, degreeScholarshipField;
  @FXML TextArea masterCourseField, masterScholarshipField;
  @FXML TextArea phdCourseField, phdScholarshipField;
  @FXML TextArea diplomaCourseField, diplomaScholarshipField;
  @FXML TextField durationField, idPrefixField;
  @FXML GridPane aboutForm, foundationForm, diplomaForm, degreeForm, masterForm, phdForm;
  @FXML VBox contentWrapper;
  List<Course> courseList = new ArrayList<>();

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    courseName.setVisible(false);
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
    
    // set button action handler
    backMain.setOnAction(e->backHome());
    updateCourse.setOnAction(e->updateInfo());
    // end set button action handler

    // set input value from csv
    try {
      getCourses();
    } catch (Exception e1) {
      msg.setText("Error has arise to attack. Call 911");
      e1.printStackTrace();
    }
    // end set input value from csv

    // remove foundation and degree form
    contentWrapper.getChildren().remove(foundationForm);
    contentWrapper.getChildren().remove(degreeForm);
    contentWrapper.getChildren().remove(masterForm);
    contentWrapper.getChildren().remove(phdForm);
    contentWrapper.getChildren().remove(diplomaForm);
  }

  /**
   * Update new course value
   */
  public void updateInfo(){
    String aboutValue = aboutField.getText();
    String durationValue = durationField.getText();
    String employmentValue = employmentField.getText();
    String idPrefixValue = idPrefixField.getText();
    String[] data = { aboutValue, durationValue, employmentValue, idPrefixValue};
    String[] foundation = {foundationCourseField.getText(), foundationScholarshipField.getText()};
    String[] degree = {degreeCourseField.getText(), degreeScholarshipField.getText()};
    String[] master = {masterCourseField.getText(), masterScholarshipField.getText()};
    String[] diploma = {diplomaCourseField.getText(), diplomaScholarshipField.getText()};
    String[] phd = {phdCourseField.getText(), phdScholarshipField.getText()};
    
    courseList.forEach(course->{
      if (course.id.equals(courseName.getText())){
        course.updateCourseInfo(data);
        course.updateSubCourseInfo(foundation, degree, master, diploma, phd);
      }
    });
    CsvWriter penulis = new CsvWriter();
    try {
      penulis.writeCourseFile(courseList);
      msg.setText("Update course done");
    } catch (Exception e) {
      msg.setText("Fail successfully. Now call for help");
      e.printStackTrace();
    }
  }

  /**
   * Initiate course class to get course data
   * @throws Exception
   */
  public void getCourses() throws Exception{
    File file = new File("course.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    for (String[] courseData : result) {
      Course tempCourse = new Course(courseData);
      courseList.add(tempCourse);
    }
    setInputValue();
  }

  /**
   * Set input value of from the previous data
   */
  public void setInputValue(){
    String name = courseName.getText();
    courseList.forEach(course->{
      if (course.id.equals(name)){
        current_course = course;
        String aboutVal = convertToNewLine(course.about);
        String employVal = convertToNewLine(course.employement_opportunities);
        aboutField.setText(aboutVal);
        durationField.setText(course.course_duration);
        employmentField.setText(employVal);
        idPrefixField.setText(course.id_prefix);
        foundationCourseField.setText(current_course.foundation[1]);
        foundationScholarshipField.setText(current_course.foundation[2]);
        degreeCourseField.setText(current_course.degree[1]);
        degreeScholarshipField.setText(current_course.degree[2]);
        masterCourseField.setText(current_course.master[1]);
        masterScholarshipField.setText(current_course.master[2]);
        phdCourseField.setText(current_course.phd[1]);
        phdScholarshipField.setText(current_course.phd[2]);
        diplomaCourseField.setText(current_course.diploma[1]);
        diplomaScholarshipField.setText(current_course.diploma[2]);
      }
    });
  }
  
  /**
   * Convert @ in string to \n for new line
   * @param value
   * @return string without @
   */
  public String convertToNewLine(String value){
    String data = "";
    String[] arrValue = value.split("@");
    data = String.join("\n", arrValue);
    return data;
  }

  /**
   * Display general form for the course information
   */
  @FXML
  private void showGeneral(){
    contentWrapper.getChildren().remove(foundationForm);
    contentWrapper.getChildren().remove(degreeForm);
    contentWrapper.getChildren().remove(diplomaForm);
    contentWrapper.getChildren().remove(masterForm);
    contentWrapper.getChildren().remove(phdForm);
    contentWrapper.getChildren().add(0, aboutForm);
  }
  
  /**
   * Display foundation form for the course information
   */
  @FXML
  public void showFoundation(){
    contentWrapper.getChildren().remove(aboutForm);
    contentWrapper.getChildren().remove(degreeForm);
    contentWrapper.getChildren().remove(diplomaForm);
    contentWrapper.getChildren().remove(masterForm);
    contentWrapper.getChildren().remove(phdForm);
    contentWrapper.getChildren().add(0, foundationForm);
  }
  
  /**
   * Display degree form for the course information
   */
  @FXML
  public void showDegree(){
    contentWrapper.getChildren().remove(aboutForm);
    contentWrapper.getChildren().remove(foundationForm);
    contentWrapper.getChildren().remove(diplomaForm);
    contentWrapper.getChildren().remove(masterForm);
    contentWrapper.getChildren().remove(phdForm);
    contentWrapper.getChildren().add(0, degreeForm);
  }
  
  /**
   * Display diploma form for the course information
   */
  @FXML
  public void showDiploma(){
    contentWrapper.getChildren().remove(aboutForm);
    contentWrapper.getChildren().remove(foundationForm);
    contentWrapper.getChildren().remove(degreeForm);
    contentWrapper.getChildren().remove(masterForm);
    contentWrapper.getChildren().remove(phdForm);
    contentWrapper.getChildren().add(0, diplomaForm);
  }
  
  /**
   * Display master form for the course information
   */
  @FXML
  public void showMaster(){
    contentWrapper.getChildren().remove(aboutForm);
    contentWrapper.getChildren().remove(foundationForm);
    contentWrapper.getChildren().remove(diplomaForm);
    contentWrapper.getChildren().remove(degreeForm);
    contentWrapper.getChildren().remove(phdForm);
    contentWrapper.getChildren().add(0, masterForm);
  }
  
  /**
   * Display phd form for the course information
   */
  @FXML
  public void showPhd(){
    contentWrapper.getChildren().remove(aboutForm);
    contentWrapper.getChildren().remove(foundationForm);
    contentWrapper.getChildren().remove(diplomaForm);
    contentWrapper.getChildren().remove(degreeForm);
    contentWrapper.getChildren().remove(masterForm);
    contentWrapper.getChildren().add(0, phdForm);
  }
  
  /**
   * Redirect user to main menu
   */
  @FXML
  public void backHome(){
    try {
      App.setRoot(folder + "menu");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}