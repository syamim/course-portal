package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import java.util.List;

import javafx.beans.property.SimpleBooleanProperty;

public class Student {
  String name, course_name, email, password, username;
  String student_id, admin_status, faculty_status;
  String last_cgpa, last_course, applied_course, id, language;
  private final SimpleBooleanProperty isSelected = new SimpleBooleanProperty();
  public Student(){};

  /**
   * Initiate student data
   * @param data
   * @param index
   */
  public Student (String[] data, int index){
    isSelected.set(false);
    id = Integer.toString(index);
    name = data[0];
    email = data[1];
    student_id = data[2];
    course_name = data[3];
    admin_status = data[4];
    faculty_status = data[5];
    last_cgpa = data[6];
    last_course = data[7];
    applied_course = data[8];
    username = data[9];
    language = data[10];
  }
  
  /**
   * Set student approval status
   * @param admin_type can be faculty or student admission
   */
  public void setData(String[] data){
    name = data[0];
    email = data[1];
    student_id = data[2];
    course_name = data[3];
    admin_status = data[4];
    faculty_status = data[5];
    last_cgpa = data[6];
    last_course = data[7];
    applied_course = data[8];
    username = data[9];
    language = data[10];
  }
  
  /**
   * Set student approval status
   * @param admin_type can be faculty or student admission
   */
  public void setStatus(String admin_type){
    if (admin_type.equals("faculty")){
      faculty_status = "approved";
      admin_status = "pending";
    }else{
      admin_status = "approved";
    }
  }

  /**
   * @param index can be 0 or 1
   * @return course applied by the student
   */
  public String getAppliedCourse(int index){
    String[] courses = applied_course.split("@");
    return courses[index];
  }
  
  /**
   * @return use to check if the checkbox status
   */
  public final boolean getIsSelected() {
    return isSelected.get();
  }

  /**
   * set the checkbox status
   * @param value
   */
  public final void setIsSelected(boolean value) {
    isSelected.set(value);
  }

  /**
   * @return return the status
   */
  public SimpleBooleanProperty isSelectedProperty() {
    return isSelected;
  }
  
  /**
   * @param index
   * @return set selected course
   */
  public String setCourse(int index){
    String[] courses = applied_course.split("@");
    course_name = courses[index-1];
    return course_name;
  }
  
  /**
   * Set student id
   * @param course_name
   * @param courses
   * @return course with new last updated id
   */
  public List<Course> setStudentId(String course_name, List<Course> courses){
    // set student id
    courses.forEach(course->{
      if(course_name.toLowerCase().contains(course.id.toLowerCase())){
        // generate id
        int new_id = Integer.parseInt(course.last_id);
        new_id++;
        student_id = course.id_prefix + Integer.toString(new_id);
        // end generate id
        
        // update last id in course
        course.last_id = Integer.toString(new_id);
        // end update last id in course
      }
    });
    // end set student id
    return courses;
  }
}