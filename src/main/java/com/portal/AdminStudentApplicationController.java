package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

public class AdminStudentApplicationController implements Initializable{
  String folder = "student_admission/";
  @FXML Label title, subtitle;
  @FXML GridPane contentWrapper;
  @FXML Button mainBtn, searchBtn;
  @FXML TextField idField;
  List<Student> studentsList = new ArrayList<>();
  
  // programmatic view item
  TableView<Student> studentListTable = new TableView<>();
  TableColumn<Student, String> idCol = new TableColumn<>("Student ID");
  TableColumn<Student, String> nameCol = new TableColumn<>("Name");
  TableColumn<Student, String> courseCol = new TableColumn<>("Course");
  TableColumn<Student, String> lastCgpaCol = new TableColumn<>("Last CGPA");
  TableColumn<Student, String> lastCourseCol = new TableColumn<>("Highest Academics");
  TableColumn<Student, String> course1Col = new TableColumn<>("Applied Course 1");
  TableColumn<Student, String> course2Col = new TableColumn<>("Applied Course 2");
  TableColumn<Student, String> languageCol = new TableColumn<>("Medium of Study");
  // end programmatic view item

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
    
    // button handler
    mainBtn.setOnAction(e->backMain());
    searchBtn.setOnAction(e->{
      try {
        searchID();
      } catch (Exception e1) {
        e1.printStackTrace();
      }
    });
    // end button handler

    // add, customise and create table
    try {
      getStudents(); // create table
    } catch (Exception e) {
      e.printStackTrace();
    }
    contentWrapper.add(studentListTable, 0,0); // add table to view
    studentListTable.setMaxHeight(300);
    studentListTable.setMinWidth(1000);
    studentListTable.setMaxWidth(1000);
    idCol.setMinWidth(100);
    nameCol.setMinWidth(300);
    courseCol.setMinWidth(150);
    lastCgpaCol.setMaxWidth(100);
    lastCourseCol.setMinWidth(150);
    idCol.setStyle("-fx-alignment: center");
    courseCol.setStyle("-fx-alignment: center");
    lastCgpaCol.setStyle("-fx-alignment: center");
    lastCourseCol.setStyle("-fx-alignment: center");
    course1Col.setStyle("-fx-alignment: center");
    course2Col.setStyle("-fx-alignment: center");
    languageCol.setStyle("-fx-alignment: center");
    // end add, customise and create table
  }
  
  /**
   * Initialize student class for the view
   * @throws Exception
   */
  public void getStudents() throws Exception{
    File file = new File("students.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    for (String[] studentData : result) {
      Student tempStudent = new Student(studentData, 0);
      studentsList.add(tempStudent);
    }
    dumpTable();
  }
  
  /**
   * Add student data to table created
   */
  public void dumpTable(){
    idCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().student_id));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    courseCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().course_name));
    lastCgpaCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().last_cgpa));
    lastCourseCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().last_course));
    course1Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(0)));
    course2Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(1)));
    languageCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().language));
    studentListTable.getColumns().addAll(idCol,nameCol,courseCol,lastCgpaCol,languageCol,lastCourseCol,course1Col,course2Col);

    studentsList.forEach(student->{
      studentListTable.getItems().add(student);
    });
  }

  /**
   * Find student with respective Student ID
   * @throws Exception
   */
  public void searchID() throws Exception{
    String id = idField.getText();
    
    File file = new File("students.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    studentsList.clear();
    for (String[] studentData : result) {
      if (id.equals(studentData[2])){
        Student tempStudent = new Student(studentData, 0);
        studentsList.add(tempStudent);
      }
    }
    
    // add data to table
    studentListTable.getItems().clear();
    studentsList.forEach(student->{
      studentListTable.getItems().add(student);
    });
  }

  /**
   * Redirect user to main menu
   */
  public void backMain(){
    try {
      App.setRoot(folder + "menu");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}