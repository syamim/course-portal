package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import javafx.fxml.Initializable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.fxml.FXML;

public class StudentCourseIDLoginController implements Initializable {
  @FXML Label courseName, durationField, employmentField, structureField, scholarshipField, aboutField;
  @FXML GridPane programContent, generalContent;
  @FXML VBox contentWrapper;
  List<Course> courseList = new ArrayList<>();
  Course current_course;
  
  @Override
  public void initialize(URL location, ResourceBundle resources) {
    courseName.setVisible(false);
    try {
      getCourses();
    } catch (Exception e1) {
      e1.printStackTrace();
    }

    // remove foundation content on initialization
    contentWrapper.getChildren().remove(programContent);
  }
  
  /**
   * Redirect user to list of course
   * @throws IOException
   */
  @FXML
  private void backCourseMenu() throws IOException {
    App.setRoot("student/courselogin");
  }
  
  /**
   * Initialize course data
   * @throws Exception
   */
  public void getCourses() throws Exception{
    File file = new File("course.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    for (String[] courseData : result) {
      Course tempCourse = new Course(courseData);
      courseList.add(tempCourse);
    }
    setValue();
  }
  
  /**
   * Set text value base on selected course
   */
  public void setValue(){
    String name = courseName.getText();
    courseList.forEach(course->{
      if (course.id.equals(name)){
        current_course = course;
        aboutField.setText(course.about);
        durationField.setText(course.course_duration);
        employmentField.setText(course.employement_opportunities);
        structureField.setText(course.foundation[1]);
        scholarshipField.setText(course.foundation[2]);
      }
    });
  }

  /**
   * Show course general info
   */
  @FXML
  public void showGeneral(){
    contentWrapper.getChildren().remove(programContent);
    contentWrapper.getChildren().add(0, generalContent);
  }
  
  /**
   * Show and set foundation info
   */
  @FXML
  public void showFoundation(){
    contentWrapper.getChildren().remove(generalContent);
    contentWrapper.getChildren().remove(programContent);
    contentWrapper.getChildren().add(0, programContent);
    structureField.setText(current_course.foundation[1]);
    scholarshipField.setText(current_course.foundation[2]);
  }
  
  /**
   * Show and set degree info
   */
  @FXML
  public void showDegree(){
    contentWrapper.getChildren().remove(generalContent);
    contentWrapper.getChildren().remove(programContent);
    contentWrapper.getChildren().add(0, programContent);
    structureField.setText(current_course.degree[1]);
    scholarshipField.setText(current_course.degree[2]);
  }
  
  /**
   * Show and set diploma info
   */
  @FXML
  public void showDiploma(){
    contentWrapper.getChildren().remove(generalContent);
    contentWrapper.getChildren().remove(programContent);
    contentWrapper.getChildren().add(0, programContent);
    structureField.setText(current_course.diploma[1]);
    scholarshipField.setText(current_course.diploma[2]);
  }

  /**
   * Show and set master info
   */
  @FXML
  public void showMaster(){
    structureField.setText(current_course.master[1]);
    scholarshipField.setText(current_course.master[2]);
    contentWrapper.getChildren().remove(generalContent);
    contentWrapper.getChildren().remove(programContent);
    contentWrapper.getChildren().add(0, programContent);
  }
  
  /**
   * Show and set phd info
   */
  @FXML
  public void showPhd(){
    contentWrapper.getChildren().remove(generalContent);
    contentWrapper.getChildren().remove(programContent);
    contentWrapper.getChildren().add(0, programContent);
    structureField.setText(current_course.phd[1]);
    scholarshipField.setText(current_course.phd[2]);
  }
}