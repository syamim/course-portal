package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import javafx.fxml.Initializable;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ResourceBundle;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.beans.binding.BooleanExpression;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;


public class StudentApplicationFormController implements Initializable {
  @FXML private ListView<String> eduListView;
  @FXML private Label eduLabel, msg, title;
  @FXML private VBox contentWrapper;
  @FXML private GridPane formWrapper;
  @FXML private TextField NameField, MailField, lastCourse, lastcgpa;
  @FXML private DatePicker BirthField;
  @FXML private CheckBox dipEngineeringCheckbox, dipComputingCheckbox, dipBusinessCheckbox, dipLawCheckbox, dipTechnologyCheckbox;
  @FXML private CheckBox foundationEngineeringCheckbox, foundationComputingCheckbox, foundationBusinessCheckbox, foundationLawCheckbox, foundationTechnologyCheckbox;
  @FXML private CheckBox degreeEngineeringCheckbox, degreeComputingCheckbox, degreeBusinessCheckbox, degreeLawCheckbox, degreeTechnologyCheckbox;
  @FXML private CheckBox masterEngineeringCheckbox, masterComputingCheckbox, masterBusinessCheckbox, masterLawCheckbox, masterTechnologyCheckbox;
  @FXML private CheckBox phdEngineeringCheckbox, phdComputingCheckbox, phdBusinessCheckbox, phdLawCheckbox, phdTechnologyCheckbox;
  @FXML private CheckBox malayLanguageCheckbox, englishLanguageCheckbox, chineseLanguageCheckbox;
  @FXML private Button editBtn, submitBtn;
  
  int totalSelected;
  int totalLanguageSelected = 0;
  Boolean isFound = false;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // check if student has applied
    try {
      if (isApplicationExist()){
        // remove form
        contentWrapper.getChildren().remove(formWrapper);
        submitBtn.setDisable(true);
        setInputValue();
        // formWrapper.setVisible(false);
      }else{
        // hide edit button
        editBtn.setVisible(false);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    // end check if student has applied

    // set listener on check box
    malayLanguageCheckbox.selectedProperty().addListener(languageChecker);
    englishLanguageCheckbox.selectedProperty().addListener(languageChecker);
    chineseLanguageCheckbox.selectedProperty().addListener(languageChecker);

    dipEngineeringCheckbox.selectedProperty().addListener(courseChecker);
    dipComputingCheckbox.selectedProperty().addListener(courseChecker);
    dipBusinessCheckbox.selectedProperty().addListener(courseChecker);
    dipLawCheckbox.selectedProperty().addListener(courseChecker);
    dipTechnologyCheckbox.selectedProperty().addListener(courseChecker);
    
    foundationEngineeringCheckbox.selectedProperty().addListener(courseChecker);
    foundationComputingCheckbox.selectedProperty().addListener(courseChecker);
    foundationBusinessCheckbox.selectedProperty().addListener(courseChecker);
    foundationLawCheckbox.selectedProperty().addListener(courseChecker);
    foundationTechnologyCheckbox.selectedProperty().addListener(courseChecker);
    
    masterEngineeringCheckbox.selectedProperty().addListener(courseChecker);
    masterComputingCheckbox.selectedProperty().addListener(courseChecker);
    masterBusinessCheckbox.selectedProperty().addListener(courseChecker);
    masterLawCheckbox.selectedProperty().addListener(courseChecker);
    masterTechnologyCheckbox.selectedProperty().addListener(courseChecker);
    
    degreeEngineeringCheckbox.selectedProperty().addListener(courseChecker);
    degreeComputingCheckbox.selectedProperty().addListener(courseChecker);
    degreeBusinessCheckbox.selectedProperty().addListener(courseChecker);
    degreeLawCheckbox.selectedProperty().addListener(courseChecker);
    degreeTechnologyCheckbox.selectedProperty().addListener(courseChecker);
    
    phdEngineeringCheckbox.selectedProperty().addListener(courseChecker);
    phdComputingCheckbox.selectedProperty().addListener(courseChecker);
    phdBusinessCheckbox.selectedProperty().addListener(courseChecker);
    phdLawCheckbox.selectedProperty().addListener(courseChecker);
    phdTechnologyCheckbox.selectedProperty().addListener(courseChecker);
    // end set listener on check box
  }

  /**
   * Set input value for all input text in the view
   * @throws Exception
   */
  public void setInputValue() throws Exception{
    List<String> lines = Files.readAllLines(Paths.get("current_user.csv"));
    String[] items = lines.get(0).split(",");
    String current_user = items[0];
    
    File file = new File("students.csv");
    CsvParser parser = new CsvParser();
    List<String[]> results = parser.readFile(file, 1);

    for (String[] studentdata : results) {
      if (current_user.equals(studentdata[9])){
        NameField.setText(studentdata[0]);
        MailField.setText(studentdata[1]);
        lastCourse.setText(studentdata[7]);
        lastcgpa.setText(studentdata[6]);
        setSelectedCourse(studentdata[8]); // check to tick the box based on the previous value
        setSelectedLanguage(studentdata[10]); // check to tick the box based on the previous value
      }
    }
  }
  
  /**
   * Check to tick the box based on the previous value
   * @param appliedCourse string of selected course
   */
  public void setSelectedLanguage(String language){
    if (language.equalsIgnoreCase("malay")){
      totalLanguageSelected++;
      malayLanguageCheckbox.setSelected(true);
    }
    if (language.equalsIgnoreCase("chinese")){
      totalLanguageSelected++;
      chineseLanguageCheckbox.setSelected(true);
    }
    if (language.equalsIgnoreCase("english")){
      totalLanguageSelected++;
      englishLanguageCheckbox.setSelected(true);
    }
  }
  
  /**
   * Check to tick the box based on the previous value
   * @param appliedCourse string of selected course
   */
  public void setSelectedCourse(String appliedCourse){
    String[] courses = appliedCourse.split("@");
    for (String course : courses) {
      if (course.toLowerCase().contains("engineering")){
        if (course.toLowerCase().contains("diploma")){
          totalSelected++;
          dipEngineeringCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("foundation")){
          totalSelected++;
          foundationEngineeringCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("degree")){
          totalSelected++;
          degreeEngineeringCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("master")){
          totalSelected++;
          masterEngineeringCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("phd")){
          totalSelected++;
          phdEngineeringCheckbox.setSelected(true);
        }
      }
      if (course.toLowerCase().contains("computing and informatics")){
        if (course.toLowerCase().contains("diploma")){
          totalSelected++;
          dipComputingCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("foundation")){
          totalSelected++;
          foundationComputingCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("degree")){
          totalSelected++;
          degreeComputingCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("master")){
          totalSelected++;
          masterComputingCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("phd")){
          totalSelected++;
          phdComputingCheckbox.setSelected(true);
        }
      }
      if (course.toLowerCase().contains("business")){
        if (course.toLowerCase().contains("diploma")){
          totalSelected++;
          dipBusinessCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("foundation")){
          totalSelected++;
          foundationBusinessCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("degree")){
          totalSelected++;
          degreeBusinessCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("master")){
          totalSelected++;
          masterBusinessCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("phd")){
          totalSelected++;
          phdBusinessCheckbox.setSelected(true);
        }
      }
      if (course.toLowerCase().contains("law")){
        if (course.toLowerCase().contains("diploma")){
          totalSelected++;
          dipLawCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("foundation")){
          totalSelected++;
          foundationLawCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("degree")){
          totalSelected++;
          degreeLawCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("master")){
          totalSelected++;
          masterLawCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("phd")){
          totalSelected++;
          phdLawCheckbox.setSelected(true);
        }
      }
      if (course.toLowerCase().contains("enginering and technology")){
        if (course.toLowerCase().contains("diploma")){
          totalSelected++;
          dipTechnologyCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("foundation")){
          totalSelected++;
          foundationTechnologyCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("degree")){
          totalSelected++;
          degreeTechnologyCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("master")){
          totalSelected++;
          masterTechnologyCheckbox.setSelected(true);
        }
        if (course.toLowerCase().contains("phd")){
          totalSelected++;
          phdTechnologyCheckbox.setSelected(true);
        }
      }
    }
  }

  /**
   * check for course selected
   */
  ChangeListener courseChecker = new ChangeListener<Boolean>() {
    @Override
    public void changed(ObservableValue<? extends Boolean> ov,Boolean old_val, Boolean new_val) {
      if (new_val){
        totalSelected++;
      }else{
        totalSelected--;
      }

      if (totalSelected > 2){
        msg.setText("Only 2 course can be select");
        submitBtn.setDisable(true);
      }else{
        submitBtn.setDisable(false);
      }
    }
  };
  
  /**
   * check for selected language
   */
  ChangeListener languageChecker = new ChangeListener<Boolean>() {
    @Override
    public void changed(ObservableValue<? extends Boolean> ov,Boolean old_val, Boolean new_val) {
      if (new_val){
        totalLanguageSelected++;
      }else{
        totalLanguageSelected--;
      }

      if (totalLanguageSelected >= 2){
        msg.setText("Only 1 Language can be select");
        submitBtn.setDisable(true);
      }else{
        msg.setText("");
        submitBtn.setDisable(false);
      }
    }
  };

  /**
   * Re-add application form into view when button edit is click
   */
  @FXML
  public void showForm(){
    contentWrapper.getChildren().remove(editBtn);
    submitBtn.setDisable(false);
    contentWrapper.getChildren().add(1, formWrapper);
  }

  /**
   * Check if student application is exist or not in database
   * @return true or false 
   * @throws Exception
   */
  public Boolean isApplicationExist() throws Exception{
    Boolean applied = false;
    List<String> lines = Files.readAllLines(Paths.get("current_user.csv"));
    String[] items = lines.get(0).split(",");
    String current_user = items[0];

    File file = new File("students.csv");
    CsvParser parser = new CsvParser();
    List<String[]> results = parser.readFile(file, 1);

    for (String[] studentdata : results) {
      if (current_user.equals(studentdata[9])){
        applied = true;
      }
    }
    return applied;
  }
  
  /**
   * Redirect user back to previous page
   * @throws IOException
   */
  @FXML
  private void backButton() throws IOException {
    App.setRoot("student/menu");
  }
  
  /**
   * Check all checkbox which is tick
   * @return return title of the checkbox
   */
  public String getSelectedLanguage(){
    String language = "Malay";
    if (malayLanguageCheckbox.isSelected()){
      language = malayLanguageCheckbox.getText();
    }
    if (englishLanguageCheckbox.isSelected()){
      language = englishLanguageCheckbox.getText();
    }
    if (chineseLanguageCheckbox.isSelected()){
      language = chineseLanguageCheckbox.getText();
    }
    return language;
  }

  /**
   * Check all checkbox which is tick
   * @return return title of the checkbox
   */
  public ArrayList<String> getSelectedCourses(){
    ArrayList<String> courses = new ArrayList<>();
    if (dipEngineeringCheckbox.isSelected()){
      courses.add(dipEngineeringCheckbox.getText());
    }
    if (dipComputingCheckbox.isSelected()){
      courses.add(dipComputingCheckbox.getText());
    }
    if (dipBusinessCheckbox.isSelected()){
      courses.add(dipBusinessCheckbox.getText());
    }
    if (dipLawCheckbox.isSelected()){
      courses.add(dipLawCheckbox.getText());
    }
    if (dipTechnologyCheckbox.isSelected()){
      courses.add(dipTechnologyCheckbox.getText());
    }
    // foundation
    if (foundationEngineeringCheckbox.isSelected()){
      courses.add(foundationEngineeringCheckbox.getText());
    }
    if (foundationComputingCheckbox.isSelected()){
      courses.add(foundationComputingCheckbox.getText());
    }
    if (foundationBusinessCheckbox.isSelected()){
      courses.add(foundationBusinessCheckbox.getText());
    }
    if (foundationLawCheckbox.isSelected()){
      courses.add(foundationLawCheckbox.getText());
    }
    if (foundationTechnologyCheckbox.isSelected()){
      courses.add(foundationTechnologyCheckbox.getText());
    }
    // degree
    if (degreeEngineeringCheckbox.isSelected()){
      courses.add(degreeEngineeringCheckbox.getText());
    }
    if (degreeComputingCheckbox.isSelected()){
      courses.add(degreeComputingCheckbox.getText());
    }
    if (degreeBusinessCheckbox.isSelected()){
      courses.add(degreeBusinessCheckbox.getText());
    }
    if (degreeLawCheckbox.isSelected()){
      courses.add(degreeLawCheckbox.getText());
    }
    if (degreeTechnologyCheckbox.isSelected()){
      courses.add(degreeTechnologyCheckbox.getText());
    }
    // master
    if (masterEngineeringCheckbox.isSelected()){
      courses.add(masterEngineeringCheckbox.getText());
    }
    if (masterComputingCheckbox.isSelected()){
      courses.add(masterComputingCheckbox.getText());
    }
    if (masterBusinessCheckbox.isSelected()){
      courses.add(masterBusinessCheckbox.getText());
    }
    if (masterLawCheckbox.isSelected()){
      courses.add(masterLawCheckbox.getText());
    }
    if (masterTechnologyCheckbox.isSelected()){
      courses.add(masterTechnologyCheckbox.getText());
    }
    // phd
    if (phdEngineeringCheckbox.isSelected()){
      courses.add(phdEngineeringCheckbox.getText());
    }
    if (phdComputingCheckbox.isSelected()){
      courses.add(phdComputingCheckbox.getText());
    }
    if (phdBusinessCheckbox.isSelected()){
      courses.add(phdBusinessCheckbox.getText());
    }
    if (phdLawCheckbox.isSelected()){
      courses.add(phdLawCheckbox.getText());
    }
    if (phdTechnologyCheckbox.isSelected()){
      courses.add(phdTechnologyCheckbox.getText());
    }
    return courses;
  }

  /**
   * Submit the form and display message depending on the situation
   * @throws Exception
   */
  @FXML
  public void submitForm() throws Exception {
    List<String> lines = Files.readAllLines(Paths.get("current_user.csv"));
    String[] items = lines.get(0).split(",");
    String username = items[0];
    ArrayList<String> courses = getSelectedCourses();

    String language = getSelectedLanguage();
    String stud_name = NameField.getText(); 
    String mail_add = MailField.getText();
    String last_course = lastCourse.getText();
    String last_cgpa = lastcgpa.getText();
    String student_id = "-";
    String course_name = "-";
    String admin_status = "pending";
    String faculty_status = "pending";
    String applied_course1 = courses.get(0);
    String applied_course2 = courses.get(1);
    
    List<String> list = Arrays.asList(applied_course1, applied_course2);
    String applied_course = String.join("@", list);
    
    String[] data = {stud_name, mail_add, student_id, course_name, admin_status, faculty_status, last_cgpa, last_course, applied_course, username, language};
    // create students array
    List<Student> studentsList = new ArrayList<>();
    File file = new File("students.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    int index = 0;
    for (String[] studentData : result) {
      index++;
      Student tempStudent = new Student(studentData, index);
      studentsList.add(tempStudent);
    }
    // end create students array
    // update data
    isFound = false;
    studentsList.forEach(student->{
      if (student.username.equals(username)){
        isFound = true;
        student.setData(data); // update approve status
      }
    });
    if (!isFound){
      Student tempStudent = new Student(data, 0);
      studentsList.add(tempStudent);
    }
    // end update data
    CsvWriter csvWriter = new CsvWriter();
    try {
      csvWriter.writeStudentFile(studentsList);
    } catch (Exception e) {   
      msg.setText("Submit form failed.");
      e.printStackTrace();
    }
    App.setRoot("student/donemenu");
  }
}


