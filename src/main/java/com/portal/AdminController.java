package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Font;

public class AdminController implements Initializable{
  String folder = "student_admission/";
  @FXML Label title;
  @FXML Label subtitle;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
  }

  /**
   * Redirect to student list to be approved view
   * @throws IOException
   */
  @FXML
  private void viewStudent() throws IOException {
    App.setRoot(folder + "view_student_info");
  }
  
  /**
   * Redirect to allocate student ID view
   * @throws IOException
   */
  @FXML
  private void allocStudID() throws IOException {
    App.setRoot(folder + "allocate_student");
  }

  /**
   * Redirect to View Student List
   * @throws IOException
   */
  @FXML
  private void viewStudentApplication() throws IOException {
    App.setRoot(folder + "view_student");
  }
  
  /**
   * Redirect to login view
   * @throws IOException
   */
  @FXML
  private void logout() throws IOException {
    App.setRoot("login");
  }
}