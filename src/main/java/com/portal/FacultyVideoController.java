package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

public class FacultyVideoController implements Initializable{
  String folder = "faculty_admin/";
  @FXML Label msg;
  @FXML TextField urlField;
  String current_user;
  List<Course> courseList = new ArrayList<>();

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    setUser();
    try {
      getCourses();
    } catch (Exception e1) {
      msg.setText("Error has arise to attack. Call 911");
      e1.printStackTrace();
    }
    // end set input value from csv
  }

  /**
   * get course based on the current user
   * @throws Exception
   */
  public void getCourses() throws Exception{
    File file = new File("course.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    for (String[] courseData : result) {
      Course tempCourse = new Course(courseData);
      courseList.add(tempCourse);
    }
    setInputValue();
  }

  /**
   * Set input field value if exist
   */
  public void setInputValue(){
    courseList.forEach(course->{
      if (course.id.toLowerCase().equals(current_user)){
        urlField.setText(course.vid_url);
      }
    });
  }
  
  /**
   * Redirect user to main menu when button is click
   */
  @FXML
  public void backMainMenu(){
    try {
      App.setRoot(folder + "menu");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Saving new url into the csv file
   * @throws IOException
   */
  @FXML
  private void saveUrl() throws IOException {
    String url = urlField.getText();
    courseList.forEach(course->{
      if (course.id.toLowerCase().equals(current_user)){
        course.setUrl(url);
      }
    });
    // saving new data
    CsvWriter writer = new CsvWriter();
    try {
      writer.writeCourseFile(courseList);
      msg.setText("New video url saved");
    } catch (Exception e) {
      msg.setText("Fail successfully. Now call for help");
      e.printStackTrace();
    }
  }

  /**
   * Get current user and set it as current_user variable
   */
  public void setUser(){
    try (Scanner sc = new Scanner(new File("current_user.csv"))) {
      while (sc.hasNext()){
        String data = sc.next();
        String[] user = data.split(",");
        if (user[2].split("_")[0].equals("faculty")){
          String[] faculty = user[2].split("_");
          current_user = faculty[2];
        }
      }
      sc.close();
    } catch (FileNotFoundException e1) {
      e1.printStackTrace();
    }
  }
}