package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CsvWriter {
  /**
   * Write student data into students.csv
   * @param students
   * @throws Exception
   */
  public void writeStudentFile(List<Student> students) throws Exception {
    FileWriter writer = new FileWriter("students.csv");
    writer.write(
      "name,email,student_id,course_name,admin_status,faculty_status,last_cgpa,last_course,applied_course,username,language"
    );
    students.forEach(student->{
      try {
        writer.write(
          "\n" + '"' + student.name + '"' + "," + 
          student.email + "," +
          student.student_id + "," +
          '"' + student.course_name + '"' + "," + 
          student.admin_status + "," +
          student.faculty_status + "," + 
          student.last_cgpa + "," + 
          '"' + student.last_course + '"' + "," + 
          '"' + student.applied_course + '"' + ","+
          '"' + student.username + '"' + "," +
          '"' + student.language + '"' + ","
        );
      } catch (IOException e) {
        System.out.println("An error while writing file.");
        e.printStackTrace();
      }
    });
    writer.close();
  }

  /**
   * Write course data into course.csv
   * @param courses
   * @throws Exception
   */
  public void writeCourseFile(List<Course> courses) throws Exception {
    FileWriter writer = new FileWriter("course.csv");
    writer.write(
      "id,name,about,course_duration,employement_opportunities,id_prefix,last_id,vid_url"
    );
    ArrayList<String> subCourseData = new ArrayList<>();
    courses.forEach(course->{
      try {
        String aboutConverted = convertMultiLineString(course.about);
        String employConverted = convertMultiLineString(course.employement_opportunities);
        writer.write(
          "\n" + '"' + course.id + '"' + "," + 
          '"' + course.name + '"' + "," + 
          '"' + aboutConverted  + '"' + "," + 
          '"' + course.course_duration + '"' + "," + 
          '"' + employConverted  + '"' + "," + 
          '"' + course.id_prefix + '"' + "," +
          '"' + course.last_id + '"' + "," +
          '"' + course.vid_url + '"' + ""
        );
      } catch (IOException e) {
        System.out.println("An error while writing file.");
        e.printStackTrace();
      }
      String foundationCourseStructureConverted = convertMultiLineString(course.foundation[1]);
      String foundationScholarshipConverted = convertMultiLineString(course.foundation[2]);
      String degreeCourseStructureConverted = convertMultiLineString(course.degree[1]);
      String degreeScholarshipConverted = convertMultiLineString(course.degree[2]);
      String diplomaCourseStructureConverted = convertMultiLineString(course.diploma[1]);
      String diplomaScholarshipConverted = convertMultiLineString(course.diploma[2]);
      String masterCourseStructureConverted = convertMultiLineString(course.master[1]);
      String masterScholarshipConverted = convertMultiLineString(course.master[2]);
      String phdCourseStructureConverted = convertMultiLineString(course.phd[1]);
      String phdScholarshipConverted = convertMultiLineString(course.phd[2]);

      subCourseData.add ('"' + course.id + '"' + "," + 
        '"' + foundationCourseStructureConverted + '"' + "," + 
        '"' + foundationScholarshipConverted + '"' + "," + 
        '"' + "foundation" + '"');
      
      subCourseData.add('"' + course.id + '"' + "," + 
        '"' + degreeCourseStructureConverted + '"' + "," + 
        '"' + degreeScholarshipConverted + '"' + "," + 
        '"' + "degree" + '"');
      
      subCourseData.add('"' + course.id + '"' + "," + 
        '"' + diplomaCourseStructureConverted + '"' + "," + 
        '"' + diplomaScholarshipConverted + '"' + "," + 
        '"' + "diploma" + '"');
      
      subCourseData.add('"' + course.id + '"' + "," + 
        '"' + masterCourseStructureConverted + '"' + "," + 
        '"' + masterScholarshipConverted + '"' + "," + 
        '"' + "master" + '"');
      
      subCourseData.add('"' + course.id + '"' + "," + 
        '"' + phdCourseStructureConverted + '"' + "," + 
        '"' + phdScholarshipConverted + '"' + "," + 
        '"' + "phd" + '"');
    });
    
    writer.close();
    subCourseWriter(subCourseData);
  }

  /**
   * Write subcourse data into subCourse.csv
   * @param data
   * @throws Exception
   */
  public void subCourseWriter(ArrayList<String> data) throws Exception {
    FileWriter writer = new FileWriter("subCourse.csv");
    writer.write(
      "id,course_structure,scholarship,type"
    );
    data.forEach(subCourse->{
      try {
        writer.write(
          "\n" + subCourse
        );
      } catch (IOException e) {
        System.out.println("An error while writing file.");
        e.printStackTrace();
      }
    });
    
    writer.close();
  }

  /**
   * Convert multiline string (\n) with @
   * @param value
   * @return return string with @
   */
  public String convertMultiLineString(String value){
    String data = "";
    String[] arrValue = value.split("\n");
    data = String.join("@", arrValue);
    return data;
  
  }

  /**
   * Write current logged in use into current_user.csv
   * @param data string of user data
   * @throws Exception
   */
  public void writeCurrentUser(String data) throws Exception {
    FileWriter writer = new FileWriter("current_user.csv");
    writer.write(data);
    writer.close();
  }
  
}