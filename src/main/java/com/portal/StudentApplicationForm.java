package com.portal;


public class StudentApplicationForm {
  String stud_name, course_name, mail_add, password;
  String student_id, admin_status, faculty_status;
  String last_cgpa, last_course, applied_course, id, username;
  
  public StudentApplicationForm () {};
  
  /**
   * Student application form data
   * @param data
   */
  public StudentApplicationForm (String[] data) {
    stud_name = data[0];
    mail_add = data[1];
    student_id = data[2];
    course_name = data[4];
    admin_status = data[5];
    faculty_status = data[6];
    last_cgpa = data[7];
    last_course = data[8];
    applied_course = data[9];
    username = data[10];
  }
}
