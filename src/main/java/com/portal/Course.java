package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.util.Arrays;
import java.util.List;

public class Course {
  String id, name, about, course_duration, employement_opportunities;
  String id_prefix, last_id, vid_url;
  String[] foundation, degree, master, phd, diploma;
  
  public Course(){};

  /**
   * Initate data for course class
   * @param data array of string that contain course data
   */
  public Course (String[] data){
    id = data[0];
    name = data[1];
    about = data[2];
    course_duration = data[3];
    employement_opportunities = data[4];
    id_prefix = data[5];
    last_id = data[6];
    vid_url = data[7];
    foundation = getSubCourse(data[0], "foundation");
    degree = getSubCourse(data[0], "degree");
    master = getSubCourse(data[0], "master");
    phd = getSubCourse(data[0], "phd");
    diploma = getSubCourse(data[0], "diploma");
  }
  
  /**
   * @param id course id
   * @param type can be foundation, degree, master, diploma or phd
   * @return return string of the subcourse depend on the course id
   */
  public String[] getSubCourse(String id, String type){
    // read get subcourse csv
    String[] data = {};
    try {
      File file = new File("subCourse.csv");
      CsvParser obj = new CsvParser();
      List<String[]> result;
      result = obj.readFile(file, 1);
      for (String[] subCourseData : result) {
        if (subCourseData[0].equalsIgnoreCase(id)){
          if (subCourseData[3].equalsIgnoreCase(type)){
            subCourseData[1] = convertToNewLine(subCourseData[1]);
            subCourseData[2] = convertToNewLine(subCourseData[2]);
            data = subCourseData;
          }
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return data;
  }

  /**
   * To update course info 
   * @param data
   */
  public void updateCourseInfo(String[] data){
    about = data[0];
    course_duration = data[1];
    employement_opportunities = data[2];
    id_prefix = data[3];
  }
  
  /**
   * update all subcourse info
   * @param diplomaData
   * @param foundationData
   * @param degreeData
   * @param masterData
   * @param phdData
   */
  public void updateSubCourseInfo(String[] foundationData, String[] degreeData, String[] masterData, String[] diplomaData, String[] phdData){
    foundation[1] = (foundationData[0] == "") ? foundation[1] : foundationData[0];
    foundation[2] = (foundationData[1] == "") ? foundation[2] : foundationData[1];
    degree[1] = (degreeData[0] == "") ? degree[1] : degreeData[0];
    degree[2] = (degreeData[1] == "") ? degree[2] : degreeData[1];
    master[1] = (masterData[0] == "") ? master[1] : masterData[0];
    master[2] = (masterData[1] == "") ? master[2] : masterData[1];
    phd[1] = (phdData[0] == "") ? phd[1] : phdData[0];
    phd[2] = (phdData[1] == "") ? phd[2] : phdData[1];
    diploma[1] = (diplomaData[0] == "") ? diploma[1] : diplomaData[0];
    diploma[2] = (diplomaData[1] == "") ? diploma[2] : diplomaData[1];
  }

  /**
   * convert from @ to \n to create new line when display data
   * @param value string to be converted
   * @return string without @
   */
  public String convertToNewLine(String value){
    String data = "";
    String[] arrValue = value.split("@");
    data = String.join("\n", arrValue);
    return data;
  }

  /**
   * set video url of the course
   * @param url
   */
  public void setUrl(String url){
    vid_url = url;
  }
}