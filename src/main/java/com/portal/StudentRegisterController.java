package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import java.io.FileWriter;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.fxml.Initializable;
import java.net.URL;
import java.util.ResourceBundle;

public class StudentRegisterController implements Initializable {

  @FXML private TextField emailField;
  @FXML private TextField passwordField;
  @FXML private Label msg;
  @Override
  public void initialize(URL location, ResourceBundle resources) {
  }
  
  /**
   * Add user data into users.csv
   * @throws IOException
   */
  @FXML
  private void register() throws IOException {
    String password = passwordField.getText();
    String email = emailField.getText();
    FileWriter writer = new FileWriter("users.csv", true);
    try {
      writer.write(
        "\n"  + email +  "," + 
        password + "," +
        "student" 
      );
    } catch (IOException e) {
      System.out.println("An error while writing file.");
      e.printStackTrace();
    }
    
    writer.close();
    msg.setText("Done Register. Please go back to log in");
  }

  /**
   * Redirect user to login view
   * @throws IOException
   */
  @FXML
  private void back() throws IOException {
    App.setRoot("login");
  }
    
}
