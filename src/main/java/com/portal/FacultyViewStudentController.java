package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.*;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

public class FacultyViewStudentController implements Initializable{
  String folder = "faculty_admin/";
  Integer courseInd;
  List<Student> studentsList = new ArrayList<>();
  String current_user = "";
  @FXML Label title, subtitle, msg;
  @FXML GridPane contentWrapper;
  @FXML Button approvedBtn, mainBtn;
  @FXML TextField idField;
  
  // programmatic view item
  TableView<Student> studentListTable = new TableView<>();
  TableColumn<Student, String> idCol = new TableColumn<>("NO");
  TableColumn<Student, String> nameCol = new TableColumn<>("Name");
  TableColumn<Student, String> courseCol = new TableColumn<>("Course");
  TableColumn<Student, String> adminCol = new TableColumn<>("Admin Approval");
  TableColumn<Student, String> facultyCol = new TableColumn<>("Faculty Approval");
  TableColumn<Student, String> course1Col = new TableColumn<>("Applied Course 1");
  TableColumn<Student, String> course2Col = new TableColumn<>("Applied Course 2");
  TableColumn<Student, String> languageCol = new TableColumn<>("Medium of Study");
  TableColumn<Student, Boolean> checkboxColumn = new TableColumn<>("Select");
  // end programmatic view item

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    setUser();
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
    
    // button handler
    approvedBtn.setOnAction(e->approvedAdmission());
    mainBtn.setOnAction(e->backMain());
    // end button handler

    // add, customise and create table
    try {
      getStudents(); // create table
    } catch (Exception e) {
      e.printStackTrace();
    }
    studentListTable.setEditable(true);
    contentWrapper.add(studentListTable, 0,0); // add table to view
    studentListTable.setMaxHeight(300);
    studentListTable.setMinWidth(800);
    idCol.setMinWidth(100);
    nameCol.setMinWidth(300);
    courseCol.setMinWidth(200);
    adminCol.setMinWidth(100);
    facultyCol.setMinWidth(100);
    idCol.setStyle("-fx-alignment: center");
    courseCol.setStyle("-fx-alignment: center");
    adminCol.setStyle("-fx-alignment: center");
    facultyCol.setStyle("-fx-alignment: center");
    course1Col.setStyle("-fx-alignment: center");
    course2Col.setStyle("-fx-alignment: center");
    languageCol.setStyle("-fx-alignment: center");
    // end add, customise and create table
  }
  
  /**
   * Initiate student class from students.csv
   * @throws Exception
   */
  public void getStudents() throws Exception{
    File file = new File("students.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    int index = 0;
    for (String[] studentData : result) {
      index++;
      Student tempStudent = new Student(studentData, index);
      studentsList.add(tempStudent);
    }
    dumpTable();
  }
  
  /**
   * Add student data into table
   */
  public void dumpTable(){
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    courseCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().course_name));
    adminCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().admin_status));
    facultyCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().faculty_status));
    course1Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(0)));
    course2Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(1)));
    languageCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().language));
    // checkbox data setting
    checkboxColumn.setCellValueFactory(new PropertyValueFactory<>("isSelected"));
    checkboxColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkboxColumn));
    checkboxColumn.setOnEditCommit(event -> {
      Student model = event.getRowValue();
      model.setIsSelected(event.getNewValue());
    });
    // end checkbox data setting
    
    studentListTable.getColumns().addAll(checkboxColumn,nameCol,courseCol,languageCol,adminCol,facultyCol,course1Col,course2Col);
    studentsList.forEach(student->{
      // check for only for faculty = course applied NOT course name by student
      if (student.getAppliedCourse(0).toLowerCase().contains(current_user)){
        courseInd = 1;
        studentListTable.getItems().add(student);
      }
      if (student.getAppliedCourse(1).toLowerCase().contains(current_user)){
        courseInd = 2;
        studentListTable.getItems().add(student);
      }
    });
  }
  
  /**
   * Update student data when button is clicked
   */
  public void approvedAdmission(){
    msg.setText("Student updated. Go back and re-enter the page");
    // find id in list and update
    studentsList.forEach(student->{
      if (student.getIsSelected()){
        student.setStatus("faculty"); // update approve status
        student.setCourse(courseInd);
      }
    });
    // end find id in list and update
    CsvWriter writerLib = new CsvWriter();
    try {
      writerLib.writeStudentFile(studentsList);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Redirect user to main menu
   */
  public void backMain(){
    try {
      App.setRoot(folder + "menu");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Get current user and set it as current_user variable
   */
  public void setUser(){
    try (Scanner sc = new Scanner(new File("current_user.csv"))) {
      while (sc.hasNext()){
        String data = sc.next();
        String[] user = data.split(",");
          if (user[2].split("_")[0].equals("faculty")){
            String[] faculty = user[2].split("_");
            current_user = faculty[2];
          }
        }
      sc.close();
    } catch (FileNotFoundException e1) {
      e1.printStackTrace();
    }
  }

}