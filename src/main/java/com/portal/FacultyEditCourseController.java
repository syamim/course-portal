package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Font;

public class FacultyEditCourseController implements Initializable{
  String folder = "faculty_admin/";
  @FXML Label title, subtitle;
  @FXML Button engineering, computing, law, business, technology;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
    
    // set button action handler
    engineering.setOnAction(e->showEditForm("Engineering"));
    computing.setOnAction(e->showEditForm("Computing"));
    law.setOnAction(e->showEditForm("Law"));
    business.setOnAction(e->showEditForm("Business"));
    technology.setOnAction(e->showEditForm("Technology"));
    // end set button action handler
    
    // hide button based on faculty
    engineering.setVisible(false);
    computing.setVisible(false);
    law.setVisible(false);
    business.setVisible(false);
    technology.setVisible(false);
    try (Scanner sc = new Scanner(new File("current_user.csv"))) {
      while (sc.hasNext()){
        String data = sc.next();
        String[] user = data.split(",");
          if (user[2].split("_")[0].equals("faculty")){
            String[] faculty = user[2].split("_");
            if (faculty[2].equals("engineering")){
              engineering.setVisible(true);  
            }
            if (faculty[2].equals("computing")){
              computing.setVisible(true);  
            }
            if (faculty[2].equals("law")){
              law.setVisible(true);  
            }
            if (faculty[2].equals("business")){
              business.setVisible(true);  
            }
            if (faculty[2].equals("technology")){
              technology.setVisible(true);  
            }
          }
        }
      sc.close();
    } catch (FileNotFoundException e1) {
      e1.printStackTrace();
    }
    // end hide button based on faculty
  }

  /**
   * Redirect user to selected form
   * @param form_name
   */
  private void showEditForm(String form_name){
    try {
      App.setRoot(folder + "form_" + form_name);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}