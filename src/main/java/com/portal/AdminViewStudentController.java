package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

public class AdminViewStudentController implements Initializable{
  String folder = "student_admission/";
  @FXML Label title, subtitle, msg;
  @FXML GridPane contentWrapper;
  @FXML Button approvedBtn, mainBtn;
  List<Student> studentsList = new ArrayList<>();
  Boolean isFound = false;
  // programmatic view item
  TableView<Student> studentListTable = new TableView<>();
  TableColumn<Student, String> idCol = new TableColumn<>("Student ID");
  TableColumn<Student, String> nameCol = new TableColumn<>("Name");
  TableColumn<Student, String> courseCol = new TableColumn<>("Course");
  TableColumn<Student, String> adminCol = new TableColumn<>("Admin Approval");
  TableColumn<Student, String> facultyCol = new TableColumn<>("Faculty Approval");
  TableColumn<Student, String> course1Col = new TableColumn<>("Applied Course 1");
  TableColumn<Student, String> course2Col = new TableColumn<>("Applied Course 2");
  TableColumn<Student, String> languageCol = new TableColumn<>("Medium of Study");
  TableColumn<Student, Boolean> checkboxColumn = new TableColumn<>("Select");
  // end programmatic view item

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
    
    // button handler
    approvedBtn.setOnAction(e->approvedAdmission());
    mainBtn.setOnAction(e->backMain());
    // end button handler

    // add, customise and create table
    try {
      getStudents(); // create table
    } catch (Exception e) {
      e.printStackTrace();
    }
    studentListTable.setEditable(true);
    contentWrapper.add(studentListTable, 0,0); // add table to view
    studentListTable.setMaxHeight(300);
    studentListTable.setMinWidth(1050);
    idCol.setMinWidth(100);
    nameCol.setMinWidth(250);
    courseCol.setMinWidth(250);
    adminCol.setMinWidth(200);
    facultyCol.setMinWidth(200);
    idCol.setStyle("-fx-alignment: center");
    courseCol.setStyle("-fx-alignment: center");
    adminCol.setStyle("-fx-alignment: center");
    facultyCol.setStyle("-fx-alignment: center");
    course1Col.setStyle("-fx-alignment: center");
    course2Col.setStyle("-fx-alignment: center");
    languageCol.setStyle("-fx-alignment: center");
    // end add, customise and create table
  }
  
  /**
   * Initialize student class for the view
   * @throws Exception
   */
  public void getStudents() throws Exception{
    File file = new File("students.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    int index = 0;
    for (String[] studentData : result) {
      index++;
      Student tempStudent = new Student(studentData, index);
      studentsList.add(tempStudent);
    }
    dumpTable();
  }
  
  /**
   * add student data into the table
   */
  public void dumpTable(){
    idCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().student_id));
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    courseCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().course_name));
    adminCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().admin_status));
    facultyCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().faculty_status));
    course1Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(0)));
    course2Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(1)));
    languageCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().language));

    // checkbox
    checkboxColumn.setCellValueFactory(new PropertyValueFactory<>("isSelected"));
    checkboxColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkboxColumn));
    checkboxColumn.setOnEditCommit(event -> {
      Student model = event.getRowValue();
      model.setIsSelected(event.getNewValue());
    });
    // end checkbox

    studentListTable.getColumns().addAll(checkboxColumn,idCol,nameCol,courseCol,languageCol,adminCol,facultyCol,course1Col,course2Col);
    studentsList.forEach(student->{
      // check if student to display student WITHOUT student id
      if(student.student_id.equals("-")){ 
        studentListTable.getItems().add(student);
      }
    });
  }

  /**
   * Update student status to approved from pending for student admission
   */
  public void approvedAdmission(){
    isFound = false;
    // find id in list and update
    studentsList.forEach(student->{
      if (student.getIsSelected()){
        isFound = true;
        student.setStatus("admin");
      }
    });
    // end find id in list and update
    if (isFound){
      CsvWriter writerLib = new CsvWriter();
      try {
        writerLib.writeStudentFile(studentsList);
        msg.setText("Student updated. Go back and re-enter the page");
      } catch (Exception e) {
        e.printStackTrace();
      }
    }else{
      msg.setText("Student not found. Go relax and come back later");
    }
  }

  /**
   * Redirect use to menu page
   */
  public void backMain(){
    try {
      App.setRoot(folder + "menu");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}