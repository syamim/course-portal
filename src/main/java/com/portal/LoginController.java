package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import java.net.URL;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import java.util.*;
import java.util.Scanner;  
import java.io.*;

public class LoginController implements Initializable {
	@FXML private PasswordField passwordField;
	@FXML private TextField idField;
  @FXML private Label error_msg;
	
  @Override
  public void initialize(URL location, ResourceBundle resources) {
  }

  /**
   * Authenticate user with based on users.csv
   */
	@FXML
	private void loginUser() throws Exception {
		Boolean userFound = false;
		String pass = passwordField.getText();
		String username = idField.getText();
    Boolean emptyInput = false;
    String user_type = "";
    // check for username first
    if (pass.equals("") || username.equals("")){
      emptyInput = true;
    }
    
    if (!emptyInput){
      Scanner sc = new Scanner(new File("users.csv"));  
      while (sc.hasNext()){
        String data = sc.next();
        String[] user = data.split(",");
        if(username.equals(user[0]) && pass.equals(user[1])) { 
          userFound = true;
          if (user[2].split("_")[0].equals("faculty")){
            setCurrentUser(data);
            user_type = "faculty_admin";
          }else if (user[2].equals("student_admission")){
            user_type = user[2];
          }else{
            user_type = user[2];
            setCurrentUser(data);
          }
        }
      }
      sc.close();
      
      // display error message
      if (!userFound){ // if user not found
        error_msg.setText("Invalid ID / Password");
      }else{ // if user found
        error_msg.setText("");
        if (user_type.equals("student")){
          App.setRoot("student/menu"); // redirect the selected course based on courseID
        }else if (user_type.equals("faculty_admin")){
          App.setRoot("faculty_admin/menu");
        }else if (user_type.equals("student_admission")){
          App.setRoot("student_admission/menu");
        }
        // end check selected course and redirect the selected course
      }
      // end display error message
    }else{
      error_msg.setText("Please fill your credential");
    }
    // end check user
  }

  /**
   * save user login info in current_user.csv
   * used all across the app the get current user status
   * @param data 
   */
  public void setCurrentUser(String data){
    CsvWriter penulis = new CsvWriter();
    try {
      penulis.writeCurrentUser(data);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Redirect user to student registration view
   * @throws IOException
   */
  @FXML
  public void signup() throws IOException {
    App.setRoot("register");
  }
}
