package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.text.Font;

public class FacultyController implements Initializable{
  String folder = "faculty_admin/";
  @FXML Label title, subtitle;
  @FXML TextField urlField;
  String current_user;

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
  }

  /**
   * Redirect user to upload video view
   * @throws IOException
   */
  @FXML
  private void uploadVideo() throws IOException {
    App.setRoot(folder + "upload_video");
  }
  
  /**
   * Redirect user to edit course view
   * @throws IOException
   */
  @FXML
  private void editCourse() throws IOException {
    App.setRoot(folder + "edit_course");
  }
  
  /**
   * Redirect user to list of student view
   * @throws IOException
   */
  @FXML
  private void viewStudentApplication() throws IOException {
    App.setRoot(folder + "view_student");
  }
  
  /**
   * Redirect user to logout view
   * @throws IOException
   */
  @FXML
  private void logout() throws IOException {
    App.setRoot("login");
  }

}