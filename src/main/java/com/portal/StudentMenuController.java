package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import javafx.fxml.Initializable;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;
import javafx.stage.Stage; 
import javafx.fxml.FXML;
import javafx.scene.web.WebView;
import javafx.scene.web.WebEngine;
import javafx.scene.Scene;
import javafx.scene.control.Label;

public class StudentMenuController implements Initializable {
  @FXML Label title;
  @Override
  public void initialize(URL location, ResourceBundle resources) {
    try {
      setName();
    } catch (Exception e) {
      System.out.println("Error 911");
    }
  }
  
  /**
   * Set student name in label 
   * @throws Exception
   */
  public void setName() throws Exception{
    List<String> lines = Files.readAllLines(Paths.get("current_user.csv"));
    String[] items = lines.get(0).split(",");
    String current_user = items[0];

    File file = new File("students.csv");
    CsvParser parser = new CsvParser();
    List<String[]> results = parser.readFile(file, 1);

    for (String[] studentdata : results) {
      if (current_user.equals(studentdata[10])){
        title.setText("Welcome ," + studentdata[0]);
      }
    }
  }

  /**
   * Redirect user to course info menu
   * @throws IOException
   */
  @FXML
  private void courseLogin() throws IOException {
    App.setRoot("student/courselogin");
  }
  
  /**
   * Redirect user to apply form view
   * @throws IOException
   */
  @FXML
  private void applyForm() throws IOException {
    App.setRoot("student/applyform");
  }
  
  /**
   * Redirect user to login view
   * @throws IOException
   */
  @FXML
  private void logOut() throws IOException {
    App.setRoot("login");
  }
  
  /**
   * Redirect user to view status of the application
   * @throws IOException
   */
  @FXML
  private void viewStatus() throws IOException {
    App.setRoot("student/status");
  }
  
  /**
   * Redirect user to course info video
   * @throws IOException
   */
  @FXML
  private void video() throws IOException {
    try {
      Stage stage = new Stage();
      // set title for the stage
      stage.setTitle("Welcome Aboard To Our Beloved University");
      
      // create a webview object
      WebView w = new WebView();
      
      // get the web engine
      WebEngine e = w.getEngine();
      
      // load a website
      //video to be changed
      e.load("https://drive.google.com/file/d/1TwE0ZMPKeE87ryjkb4XoPGxdhj7vp4pS/view?usp=sharing");
      
      // create a scene
      Scene scene = new Scene(w, w.getPrefWidth(), 
      w.getPrefHeight());
      
      // set the scene
      stage.setScene(scene);
      
      stage.show();
    }
    catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }
}


