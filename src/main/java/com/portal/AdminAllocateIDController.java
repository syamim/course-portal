package com.portal;
/*
 * @author Rino bin Marsono 1211303721.
 * 
 * @author Muhammad Syamim Bin Mahamad Shabudin 1211304012.
 * 
 */
import java.io.*;
import java.net.URL;
import java.util.*;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;

public class AdminAllocateIDController implements Initializable{
  String folder = "student_admission/";
  Boolean isFound = false;
  @FXML Label title;
  @FXML Label subtitle;
  @FXML Label msg;
  @FXML GridPane contentWrapper;
  @FXML GridPane formWrapper;
  @FXML Button approvedBtn;
  @FXML Button mainBtn;
  @FXML TextField idField;
  List<Student> studentsList = new ArrayList<>();
  List<Student> selectedStudentList = new ArrayList<>();
  List<Course> courseList = new ArrayList<>();
  
  // programmatic view item
  TableView<Student> studentListTable = new TableView<>();
  TableColumn<Student, String> nameCol = new TableColumn<>("Name");
  TableColumn<Student, String> courseCol = new TableColumn<>("Course");
  TableColumn<Student, String> lastCgpaCol = new TableColumn<>("Last CGPA");
  TableColumn<Student, String> lastCourseCol = new TableColumn<>("Highest Academics");
  TableColumn<Student, String> course1Col = new TableColumn<>("Applied Course 1");
  TableColumn<Student, String> course2Col = new TableColumn<>("Applied Course 2");
  TableColumn<Student, Boolean> checkboxColumn = new TableColumn<>("Select");
  // end programmatic view item

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    // set style
    title.setFont(new Font("Arial", 20));
    title.setStyle("-fx-font-weight: BOLD");
    subtitle.setFont(new Font("Arial", 14));
    // end set style
    
    // button handler
    approvedBtn.setOnAction(e->allocateID());
    mainBtn.setOnAction(e->backMain());
    // end button handler

    // add, customise and create table
    try {
      getStudents(); // create table
      getCourses(); // initiate course class to pass into student
    } catch (Exception e) {
      e.printStackTrace();
    }
    
    studentListTable.setEditable(true);
    contentWrapper.add(studentListTable, 0,0); // add table to view
    studentListTable.setMaxHeight(300);
    studentListTable.setMinWidth(1000);
    studentListTable.setMaxWidth(1000);
    nameCol.setMinWidth(300);
    courseCol.setMinWidth(150);
    lastCgpaCol.setMaxWidth(100);
    lastCourseCol.setMinWidth(150);
    lastCourseCol.setMinWidth(150);
    courseCol.setStyle("-fx-alignment: center");
    lastCgpaCol.setStyle("-fx-alignment: center");
    lastCourseCol.setStyle("-fx-alignment: center");
    course1Col.setStyle("-fx-alignment: center");
    course2Col.setStyle("-fx-alignment: center");
    // end add, customise and create table
   
  }

  /**
   * Read all student data
   * @throws Exception
   */
  public void getStudents() throws Exception{
    File file = new File("students.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    int id = 0;
    for (String[] studentData : result) {
      id++;
      Student tempStudent = new Student(studentData, id);
      studentsList.add(tempStudent);
    }
    dumpTable();
  }
  
  /**
   * add data into the table
   */
  public void dumpTable(){
    nameCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().name));
    courseCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().course_name));
    lastCgpaCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().last_cgpa));
    lastCourseCol.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().last_course));
    course1Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(0)));
    course2Col.setCellValueFactory(data -> new ReadOnlyStringWrapper(data.getValue().getAppliedCourse(1)));
    
    // checkbox
    checkboxColumn.setCellValueFactory(new PropertyValueFactory<>("isSelected"));
    checkboxColumn.setCellFactory(CheckBoxTableCell.forTableColumn(checkboxColumn));
    checkboxColumn.setOnEditCommit(event -> {
      Student model = event.getRowValue();
      model.setIsSelected(event.getNewValue());
    });
    // end checkbox
    studentListTable.getColumns().addAll(checkboxColumn,nameCol,courseCol,lastCgpaCol,lastCourseCol,course1Col,course2Col);

    studentsList.forEach(student->{
      // check if student to display student WITHOUT student id only
      if(student.student_id.equals("-") && !student.course_name.equals("-")){ 
        studentListTable.getItems().add(student);
      }
    });
  }

  /**
   * Set student id when button is click
   */
  public void allocateID(){
    // find id in list and update
    studentsList.forEach(student->{
      if (student.getIsSelected()){
        courseList = student.setStudentId(student.course_name, courseList);
      }
    });
    // end find id in list and update
    CsvWriter writerLib = new CsvWriter();
    try {
      writerLib.writeStudentFile(studentsList);
      writerLib.writeCourseFile(courseList);
      msg.setText("Student updated. Go back and re-enter the page");
    } catch (Exception e) {
        msg.setText("Some error occured. Call 911");
        e.printStackTrace();
      }
  }

  /**
   * Create course class to be link with the student 
   * @throws Exception
   */
  public void getCourses() throws Exception{
    File file = new File("course.csv");
    CsvParser obj = new CsvParser();
    List<String[]> result = obj.readFile(file, 1);
    for (String[] courseData : result) {
      Course tempCourse = new Course(courseData);
      courseList.add(tempCourse);
    }
  }

  /**
   * Redirect use to menu page
   */
  public void backMain(){
    try {
      App.setRoot(folder + "menu");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}