package com.portal;
/*
 * @author Nur Damia Binti Rohisyam 1211305018.
 * 
 * @author Faqihah Binti Zakir 1211303109.
 * 
 * @author Nur Anis Nabila Binti Mohd Romzi 1211303587.
 * 
 */
import java.io.File;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.fxml.Initializable;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class StudentController implements Initializable{
  @FXML Label stat, congrats, error_msg;
  @FXML TextField emailField, passwordField;
  List<Student> studentsList = new ArrayList<>();
  
  @Override
  public void initialize(URL location, ResourceBundle resources) {
    try {
      setLabelValue(); // create table
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * Find student based on current user to set label value
   * @throws Exception
   */
  public void setLabelValue() throws Exception{
    List<String> lines = Files.readAllLines(Paths.get("current_user.csv"));
    String[] items = lines.get(0).split(",");
    String current_user = items[0];
    File file = new File("students.csv");
    CsvParser parser = new CsvParser();
    List<String[]> results = parser.readFile(file, 1);

    for (String[] studentdata : results) {
      if (current_user.equals(studentdata[9])){
        stat.setText(studentdata[4]);
        error_msg.setText(studentdata[3]);
      }else{
        stat.setText("Under Review");
        error_msg.setText("Under Review");
      }

      if (studentdata[5] == "pending" ){
        error_msg.setText("Not yet");
      }
    }
  }
  
  /**
   * Redirect user to main menu
   * @throws IOException
   */
  @FXML
  private void backMenu() throws IOException {
    App.setRoot("student/menu");
  }

}