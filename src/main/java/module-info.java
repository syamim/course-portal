module com.portal {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;

    opens com.portal to javafx.fxml;
    exports com.portal;
}