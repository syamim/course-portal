# Course Portal
![Application Preview](images/system_preview.png?raw=true "Application Preview")
This is a course portal build with java as an assignment for 
Object Oriented and Analysis Design course. The Course Selection Portal is an online resource for a Self-financing multi-disciplinary university (SFMDU) and students looking for their programme of study.

## Course Info Form Detail
### General Capabilities
✅ Login <br/>
✅ Student Registration

### Student Admission Capabilities
✅ Allocate Student ID <br/>
✅ View all student information <br/>
✅ Update Student Application Status <br/>

### Student Admission 
✅ Upload video briefing <br/>
✅ Edit/Update course infomation <br/>
✅ Update Student Application Status <br/>

### Student Admission 
✅ View briefing video <br/>
✅ View course info <br/>
✅ Apply for course <br/>
✅ View application status <br/>

## Getting Started
### Prerequisites
System requirement to run the java application is
 * OpenJDK 18.0.2 and above [Official Website](https://jdk.java.net/19/)
 * Apache Maven 3.8.6 and above [Official Website](https://maven.apache.org/download.cgi)
### Compile and Run
Compiling and running the application
1. Clone the project from the link using `git clone` or you can download the zip file from this repository
2. Open command line of your machine and navigate to the downloaded or cloned document
![Window Navigation Example](images/windows_screenshot.jpg?raw=true "Window Navigation")
3. From the root folder of the project folder, run `mvn clean javafx:run` and wait for the installation to finish and the app will running
4. If you don’t have `mvn` installed, you can follow [this tutorial](https://phoenixnap.com/kb/install-maven-windows) to add mvn into your machine for windows.
5. For macOs and linux user, you can add mvn package by downloading executable file from maven official website [Official Website](https://maven.apache.org/download.cgi)
6. Then add it to your environment in `.zshrc` or environment variable of the system like below
![MacOs/Linux Navigation Example](images/linux_screenshot.png?raw=true "Window Navigation")
